---
layout: people
name: Cesar Giacomini Penteado
role: Professores
lattes:
  id: "5160104822250783"
summary: |
  Doutorado em Engenharia Elétrica pela Universidade de São Paulo, Brasil(2010)  
  Professor da Universidade de Marília , Brasil
---

Possui graduação em Ciência da Computação pelo Centro Universitário Eurípedes de Marília (2002) e mestrado em Ciência da Computação pelo Centro Universitário Eurípedes de Marília (2004). Doutor em Engenharia Elétrica na Escola Politécnica da Universidade de São Paulo (2010), São Paulo, Pós Doutorado (2022) no Instituto de Física da Universidade de São Paulo e foi projetista de Circuitos Integrados (digitais) na Design House do LSITEC, Bolsista do CNPq em Desenvolvimento Tecnológico em Semicondutores por 5 anos. Tem experiência na área de Engenharia Elétrica, com ênfase em Eletrônica Industrial, atuando principalmente nos seguintes temas: ferramentas cadence, fpga, automação, microcontrolador, vhdl e lógica digital.

---
layout: people
name: Ettore Leandro Tognoli
role: Professores
github:
  username: ettoreleandrotognoli
gitlab:
  username: ettoreleandrotognoli
linkedin:
  username: ettore-leandro-tognoli
email:
  address: ettoretognoli@unimar.br
lattes:
  id: "1563628701928365"
summary: |
  Mestrado em Ciência da Computação pela Universidade Federal de São Carlos, Brasil(2016)  
  Engenheiro de Software na Red Hat , Brasil  
  Professor da Universidade de Marília , Brasil  
---

Possui graduação em Ciência da Computação pelo Centro Universitário Euripedes de Marília (2012) e mestrado em Ciência da Computação pela Universidade Federal de São Carlos (2016). Atualmente é engenheiro de software na Red Hat.

---
layout: post
title: "Confecção de PCIs com CNC"
date: 2024-11-18 00:00:00 -0300
summary: |
  Este artigo explora a fabricação de placas de circuito impresso (PCIs) utilizando máquinas CNC, destacando-a como uma alternativa precisa e sustentável aos métodos tradicionais de transferência térmica e corrosão com ácido. O processo elimina o uso de produtos químicos, reduzindo impactos ambientais, e oferece maior precisão e flexibilidade para prototipagem e personalização. Ideal para makerspaces e laboratórios, a abordagem com CNC apresenta uma solução eficiente para a produção de PCIs com qualidade profissional.
categories: pci cnc
authors:
  - name: Ettore Leandro Tognoli
license:
  name: CC BY-NC 4.0
  url: https://creativecommons.org/licenses/by-nc/4.0/
---

A fabricação de placas de circuito impresso (PCIs) é uma etapa crucial para o desenvolvimento de projetos eletrônicos, especialmente em contextos educacionais, makerspaces e pequenos laboratórios. Tradicionalmente, o processo de produção envolve métodos como a transferência térmica e a corrosão com ácido. Embora amplamente utilizados, esses métodos apresentam limitações significativas: a transferência térmica pode gerar imprecisões na reprodução do design, enquanto o uso de ácidos, além de perigoso, resulta na produção de resíduos tóxicos que requerem descarte adequado.

Como alternativa a esses métodos, o uso de máquinas CNC vem ganhando destaque. A tecnologia permite a usinagem direta do cobre em placas virgens, eliminando a necessidade de produtos químicos e proporcionando maior precisão e controle sobre o processo. Neste artigo, optamos por utilizar uma CNC 3018 devido à sua acessibilidade e facilidade de uso, tornando-a ideal para iniciantes e pequenos projetos. Além disso, para tornar o processo mais acessível, empregaremos softwares de código aberto para o design dos circuitos e a geração do código G necessário para a usinagem.

## CNC 3018

A CNC 3018 é uma máquina de usinagem compacta e acessível, amplamente utilizada por hobbistas, estudantes e makers devido à sua versatilidade e custo reduzido. Projetada principalmente para gravação em madeira, plástico e metais leves, a CNC 3018 também pode ser adaptada para a fabricação de placas de circuito impresso (PCIs). Ela possui uma estrutura simples, normalmente construída em alumínio, e é equipada com um spindle que permite a utilização de fresas de diferentes tamanhos para trabalhos precisos. Além disso, sua compatibilidade com softwares de código aberto, como GRBL e Candle, facilita a integração com fluxos de trabalho de prototipagem. Embora suas limitações incluam uma área de trabalho relativamente pequena e menor robustez em comparação com máquinas industriais, a CNC 3018 é ideal para quem busca aprender e explorar processos de usinagem em pequena escala, como a criação de PCIs.

A 3018 adiquira veio pré-montada, exigindo poucos passos para a montagem.
Na figura a seguir podemos ver seus componentes, sendo que a base, eixo Y, e o eixo Z com a router já vieram montados.

![]({{site.baseurl}}/assets/pci/3018-unbox.jpg)

Na figura a seguir podemos ver a 3018 completamente montada.

![]({{site.baseurl}}/assets/pci/3018-montada.jpg)

Na confecção de placas de circuito impresso (PCIs) com CNC, a escolha adequada das fresas (bits) é essencial para garantir a precisão do trabalho e a durabilidade das ferramentas. A opções mais comum são as fresas tipo _V-bit_. Para o isolamento das trilhas, as fresas _V-bit_ são amplamente utilizadas devido à sua capacidade de criar cortes precisos em áreas estreitas, com ângulos variando entre 10° e 90° dependendo do nível de detalhe desejado. Além disso, fresas com revestimento de carbeto de tungstênio são preferidas por sua alta durabilidade e resistência ao desgaste. A seleção da fesa deve considerar o tipo de design, o material da placa e os parâmetros de corte, garantindo um resultado eficiente e de alta qualidade.

Na figura a seguir podemos ver fresas do tipo piraimidal de 20° até 90°.

![]({{site.baseurl}}/assets/pci/fresa-piramidal.png)

Na figura a seguir podemos ver fresas do tipo vbit de 10° até 90°.

![]({{site.baseurl}}/assets/pci/fresa-vbit.png)

O **probe** na CNC é uma ferramenta de medição utilizada para definir com precisão a posição do ponto zero (referência) da peça a ser usinada, especialmente no eixo Z. Ele normalmente funciona como um contato elétrico, onde um sensor detecta o contato entre o probe e a superfície da peça, criando um circuito elétrico. Para que o probe funcione corretamente, o material da peça precisa conduzir eletricidade, permitindo que o sensor registre o contato. Quando o probe entra em contato com a superfície, ele envia um sinal para a CNC, que ajusta automaticamente sua posição para garantir que o corte ou perfuração seja feito na profundidade exata. Esse processo é fundamental para compensar variações na altura da peça, assegurando precisão e consistência no processo de usinagem, especialmente em aplicações como a fabricação de placas de circuito impresso, onde a exatidão é crucial para o sucesso do projeto.

![]({{site.baseurl}}/assets/pci/probe.webp)

## Universal G-Code Sender

O [Universal G-code Sender (UGS)](https://github.com/winder/Universal-G-Code-Sender) é uma aplicação de código aberto amplamente utilizada para controlar máquinas CNC baseadas em GRBL. Ele oferece uma interface intuitiva para enviar comandos G-code, facilitando a operação de máquinas como a CNC 3018. Desenvolvido em Java, o UGS é compatível com múltiplos sistemas operacionais e oferece funcionalidades robustas, como visualização em 3D do código G antes da execução, configuração precisa de parâmetros da máquina e monitoramento em tempo real.

![]({{site.baseurl}}/assets/pci/ugs-home.png)

Uma das características essenciais do UGS para a confecção de PCIs é o plugin "Auto Leveler", que permite nivelar automaticamente a superfície da placa, corrigindo variações de altura e garantindo que o desgaste do cobre seja realizado com alta precisão. Essa funcionalidade é fundamental, pois em PCIs, é crucial remover o cobre de maneira uniforme para preservar a integridade dos circuitos. Sua popularidade se deve à simplicidade de uso, combinada com ferramentas avançadas que atendem tanto iniciantes quanto usuários experientes. No contexto de fabricação de PCIs, o UGS se torna uma ferramenta indispensável para garantir a qualidade e a precisão do processo de usinagem.

Na figura a seguir, podemos observar o resultado de cortes excessivamente profundos, especialmente no lado esquerdo. As trilhas e ilhas ficam extremamente finas, ou até foram removidas, o que dificulta a perfuração e compromete a integridade da placa.
Também podemos notar que alguns isolamentos não foram completamente desgastados, resultado de um nivelamento ruim ou do não nivelamento.

![]({{site.baseurl}}/assets/pci/pci.jpg)

A popularidade do UGS se deve à simplicidade de uso, combinada com ferramentas avançadas que atendem tanto iniciantes quanto usuários experientes. No contexto de fabricação de PCIs, o UGS se torna uma ferramenta indispensável para garantir a qualidade e a precisão do processo de usinagem.

## KiCad

Existem diversas opções de softwares para o desenvolvimento de projetos eletrônicos, sendo o [Eagle](<https://en.wikipedia.org/wiki/EAGLE_(program)>) e o [KiCad](https://www.kicad.org/) os mais populares entre os entusiastas e profissionais. Ambos são amplamente utilizados para o design de esquemas e layout de PCIs, oferecendo ferramentas avançadas para criar circuitos complexos. Além disso, há outras alternativas como **Altium Designer**, **EasyEDA** e **Autodesk Fusion 360**, que também oferecem funcionalidades robustas para o design eletrônico. Esses softwares normalmente exportam arquivos no formato **Gerber (GBR)** e **Drill (DRL)**, que contêm as informações necessárias para a fabricação das placas, como as camadas de cobre e os furos de perfuração. Esses arquivos podem ser convertidos em **G-code** por programas como o **FlatCAM**, que realiza a conversão dos dados para que a máquina CNC possa usá-los no processo de usinagem. Para o nosso projeto, optamos pela utilização do **KiCad**, pois além de ser uma ferramenta de código aberto, é amplamente utilizada por sua flexibilidade e compatibilidade com diversos formatos, o que facilita a integração com outros programas e a personalização do processo de fabricação de PCIs.

![]({{site.baseurl}}/assets/pci/kicad-home.png)

## FlatCAM

O [FlatCAM](http://flatcam.org/) é uma ferramenta de código aberto projetada especificamente para converter arquivos Gerber (GBR) e Drill (DRL) em G-code, que pode ser utilizado por máquinas CNC. Ele é amplamente utilizado no processo de fabricação de placas de circuito impresso (PCIs), pois permite transformar o design da placa, criado em softwares como KiCad ou Eagle, em instruções precisas para a usinagem. O FlatCAM oferece uma interface simples e intuitiva, permitindo ao usuário ajustar parâmetros como a profundidade de corte, a largura das trilhas e a configuração das fresas. Além disso, o software permite o controle de diversas camadas do circuito, garantindo que o processo de usinagem ocorra de maneira eficiente e precisa. Com sua capacidade de gerar G-code para corte de cobre e perfuração, o FlatCAM é uma ferramenta essencial para quem deseja usar uma máquina CNC na fabricação de PCIs de maneira prática e acessível.

![]({{site.baseurl}}/assets/pci/flatcam-home.png)

## Roteiro de Confecção

1. **Design do Projeto**  
   Comece criando o design do circuito eletrônico utilizando um software de design de PCIs, como o KiCad ou Eagle. Desenhe o esquema elétrico e, em seguida, o layout da placa com as trilhas e componentes.

   ![]({{site.baseurl}}/assets/pci/kicad-01.png)

   ![]({{site.baseurl}}/assets/pci/kicad-02.png)

2. **Exportar os Arquivos Gerber (GBR) e Drill (DRL)**  
   Após concluir o design, exporte os arquivos Gerber (GBR) e Drill (DRL) a partir do software de design. O arquivo Gerber contém as informações das camadas de cobre, enquanto o arquivo Drill contém os dados dos furos de perfuração.

   ![]({{site.baseurl}}/assets/pci/kicad-03.png)

3. **Importar Arquivos Gerber no FlatCAM**  
   Abra o FlatCAM e importe os arquivos Gerber (.gbr) para preparar a usinagem da placa. O FlatCAM irá interpretar os arquivos e permitir que você ajuste as configurações para o processo de usinagem.

   ![]({{site.baseurl}}/assets/pci/flatcam-01.png)

4. **Escolher o Bit e Gerar Geometria**  
   Selecione o bit (fresa) adequado para o trabalho, dependendo do tamanho das trilhas e do design da placa. O FlatCAM irá gerar a geometria a partir do arquivo Gerber, configurando as áreas a serem usinadas.

   ![]({{site.baseurl}}/assets/pci/flatcam-02.png)

5. **Definir Profundidade, Velocidade e Gerar G-code**  
   Defina os parâmetros de usinagem, como a profundidade de corte, a velocidade de avanço e a rotação da fresa. Com as configurações definidas, o FlatCAM gerará o código G que será utilizado pela CNC para o corte da placa.

   ![]({{site.baseurl}}/assets/pci/flatcam-02.png)

6. **Importar Arquivo Drill (DRL) no FlatCAM**  
   Importe o arquivo Drill (.drl) no FlatCAM para gerar o G-code necessário para a perfuração dos furos na placa, como os de componentes e vias.

7. **Gerar G-code para Perfuração**  
   Com o arquivo Drill importado, defina os parâmetros de perfuração, como o tamanho da broca e a profundidade dos furos. O FlatCAM gerará o G-code para essa parte do processo.

8. **Carregar G-code no Universal G-code Sender (UGS)**  
   Abra o Universal G-code Sender (UGS) e carregue o G-code gerado para a usinagem das trilhas.

   ![]({{site.baseurl}}/assets/pci/ugs-gcode.png)

9. **Colocar o Bit Correto na CNC**  
   Substitua a fresa da CNC pela fresa apropriada para o corte das trilhas.

10. **Posicionar a Placa Virgem na CNC**  
    Coloque a placa virgem de cobre na área de trabalho da CNC, alinhando-a de maneira que o centro da placa esteja corretamente posicionado.

    ![]({{site.baseurl}}/assets/pci/placa-virgem.jpg)
    
    É extremamente importante que a placa esteja bem fixada, pois qualquer deslocamento causado pelo movimento da fresa pode comprometer o processo de usinagem e arruinar o resultado final.

11. **Definir Ponto Zero**  
    Defina o ponto zero (0,0,0) na CNC, garantindo que o sistema saiba a posição de referência para o início do processo de usinagem.

    ![]({{site.baseurl}}/assets/pci/xy-zero.jpg)

12. **Fazer Probe para Encontrar o Z Zero**  
    Utilize a função de probe da CNC para medir a altura exata da superfície da placa, garantindo que o eixo Z esteja corretamente posicionado para o corte.

    ![]({{site.baseurl}}/assets/pci/probe.jpg)

13. **Fazer Nivelamento com o Plugin "Auto Leveler"**  
    Use o plugin "Auto Leveler" no UGS para nivelar automaticamente a superfície da placa. Isso ajuda a corrigir pequenas variações de altura da placa, garantindo um desgaste uniforme do cobre.

    ![]({{site.baseurl}}/assets/pci/auto-leveler.png)

14. **Iniciar o Trabalho e Aguardar**  
    Inicie o processo de usinagem no UGS e aguarde enquanto a CNC realiza o corte das trilhas na placa. Este é o momento em que as trilhas do circuito começam a ser removidas.

    ![]({{site.baseurl}}/assets/pci/inicio.jpg)

    Foi aplicado óleo de máquina na superfície da placa. O óleo ajuda a capturar a maior parte das partículas geradas durante a usinagem, reduzindo a suspensão de poeira no ar, além de manter a fresa lubrificada e resfriada durante o corte.

15. **Trocar V-bit pela Broca para os Furos**  
    Quando a usinagem das trilhas estiver concluída, troque a fresa V-bit pela broca adequada para perfurar os furos.

16. **Carregar G-code para Perfuração**  
    Carregue o G-code gerado para a perfuração dos furos no UGS. Certifique-se de que todos os furos necessários estão incluídos no arquivo.

17. **Iniciar o Trabalho de Perfuração e Aguardar**  
    Inicie o trabalho de perfuração e aguarde enquanto a CNC perfura os furos da placa. Pode ser necessário realizar trocas de broca durante o processo, dependendo do tamanho dos furos.

Agora que você já teve contato com as ferramentas e o processo necessário para a confecção da sua própria PCI, espero que você consiga colocar todo esse conhecimento em prática. Ao longo de seus projetos, você pode encontrar novos desafios, como ajustes finos nas configurações de corte, problemas com o design ou até mesmo questões relacionadas ao acabamento da placa. No entanto, cada desafio será uma oportunidade de aprendizado e aprimoramento, permitindo que você se torne mais experiente e confiante em seus projetos eletrônicos. Lembre-se de que a prática é fundamental e, com o tempo, você dominará as técnicas para criar PCIs com alta qualidade e precisão. Boa sorte e bons projetos!

## Material de Apoio

[Curso de Projeto de PCB com KiCAD - Prof. Rodrigo Tavares](https://www.youtube.com/playlist?list=PL8kCnofYYWZrkhUis1svLGux1tjYNJy2Z)

[FlatCAM PCB CNC Full Tutorial - ECH BROS](https://www.youtube.com/watch?v=--Cb11heuHc)

[FlatCam 2 sided PCB milling on a CNC - ECH BROS](https://www.youtube.com/watch?v=8cGf6NvubFk)

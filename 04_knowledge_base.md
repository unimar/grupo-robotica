---
layout: default
title: Links Educacionais
permalink: /links/
---


## Rust

<https://www.rust-lang.org/learn>  
<https://doc.rust-lang.org/book/>  
<https://doc.rust-lang.org/cargo/>  

### Embedded Rust

<https://www.youtube.com/watch?v=x7LQevYn7d0>

---

## C/C++

<https://greenteapress.com/thinkcpp/index.html>  
<https://opendatastructures.org/ods-cpp/>  

---

## Git

<https://git-scm.com/book/pt-br/v2>

---

## Diversos

[Curso Manual Maker - Manual do Mundo](https://www.youtube.com/playlist?list=PLYjrJH3e_wDNLUTN32WittrpBxeleEqNp)  

[Programação de sistemas embarcados - Rodrigo Maximiano Antunes de Almeida](
https://www.youtube.com/playlist?list=PLqBAJMdCNemmW6nC3g5TGFBywLqy2UBnB)  

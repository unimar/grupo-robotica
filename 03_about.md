---
layout: page
title: Sobre
permalink: /about/
---

Explore o mundo da robótica e automação conosco! Nosso site é o destino definitivo para entusiastas, estudantes e profissionais interessados na interseção entre robótica, ciência da computação, análise e desenvolvimento de sistemas, engenharia de produção mecânica e engenharia elétrica. Oferecemos uma rica fonte de informações, projetos inspiradores e tutoriais detalhados que capacitam você a mergulhar de cabeça no universo da robótica.

Nossas parcerias com instituições acadêmicas de renome, incluindo cursos de bacharelado em Ciência da Computação, Análise e Desenvolvimento de Sistemas, Engenharia de Produção Mecânica e Engenharia Elétrica, proporcionam uma base sólida para nosso conteúdo. Trabalhamos em estreita colaboração com essas instituições para promover o aprendizado prático e a aplicação de conhecimentos em projetos de robótica e automação de ponta.

Explore nossos recursos, participe de nossa comunidade e descubra como a robótica está transformando o mundo em diversas áreas, desde manufatura e automação residencial até assistência médica e muito mais. Junte-se a nós nessa jornada emocionante em direção ao futuro da tecnologia e inovação.

---
layout: default
title: Time
permalink: /team/
---

## Parceiros

{% include sponsor-list.html page=site %}

<br/>

<ul class="post-list">
    {% assign roles = site.people | group_by: "role" %}
    {% for role in roles %}
    <li class="role">
        <h2>{{ role.name | capitalize }}</h2>
    </li>
    {% for person in role.items %}
    <li class="person">
        <div>
            <a href="{{person.url | relative_url }}">{{person.name}}</a>
            {% include social-list.html page=person %}
            {% if person.summary %}
            <div class="small">
            {{person.summary | markdownify}}
            </div>
            {% endif %}
        </div>
    </li>
    {% endfor %}
    {% endfor %}
</ul>

![Build Status](https://gitlab.com/unimar/grupo-robotica/badges/main/pipeline.svg)

---

<https://unimar.gitlab.io/grupo-robotica/>

---

## Developer Guide

Run it locally:

```sh
docker compose up
```

## Quero aparecer no site

Basta criar um arquivo com seu perfil na pasta `_people` e nos mandar um merge request.  
Verique os perfis já existentes para saber o que fazer.

## Quero publicar um artigo no site

Basta criar um arquivo com seu artigo na pasta `_posts` e nos mandar um merge request.
